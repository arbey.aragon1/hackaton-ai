sudo docker kill $(sudo docker ps -a -q)
sudo docker rm $(sudo docker ps -a -q)
sudo docker build -t tt:1 .
sudo docker run -it \
    --name tensorflowTest \
    -p 8888:8888 \
    -p 5000:5000 \
    -p 6006:6006 -d \
    -v $(pwd)/project:/home/jovyan/project \
    tt:1 bash

sudo docker exec -it tensorflowTest bash

